var tkn = localStorage.getItem("auth_token");
var usrName = localStorage.getItem("username");
console.log('This is getToken script');
//if (window.location.hostname = "bdsr-uat-trims-web01.dc1.lan") { devMode = true}
chrome.storage.sync.get(["devMode"], function(result) {
  console.log("The devMode is " + result["devMode"])
  window.devMode = result["devMode"];
});
//devMode = false;
if (window.devMode) {
  chrome.storage.sync.set({uatToken: tkn}, function() {     
    console.log("The tkn value is set now: " + tkn );
  });
  chrome.storage.sync.set({uatName: usrName}, function() {     
    console.log("The username value is set now: " + usrName );
  });
} else {
  chrome.storage.sync.set({prdToken: tkn}, function() {     
    console.log("The tkn value is set now: " + tkn );
  });
  chrome.storage.sync.set({prdName: usrName}, function() {     
    console.log("The username value is set now: " + usrName );
  });
}

