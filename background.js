var dev = false;
//var dev = true;
chrome.storage.sync.set({devMode: dev}, function() {     
  console.log("The devMode value is set now: " + dev );
});
if (dev) {
  var baseUrl = 'https://bdsr-uat-trims-web01.dc1.lan/api/';
  userName = "uatName";
  trimsToken = "uatToken";
}
else {
  var baseUrl = 'https://prd-bdsr-trims-web01.dc2.lan/api/';
  userName = "prdName";
  trimsToken = "prdToken";
}

var queryVuln = 'vulns/?&status=DRAFT&returned=TRUE&owned_by=';
var queryScrp = 'scrapedvulns/?status=ASSIGNED&owned_by=';
function trimsRequest (token, query) {
  this.token = token;
  this.query = query;
  var xhr = new XMLHttpRequest();
  xhr.open('GET', baseUrl + query, true);    //true: Asynchronous
  xhr.setRequestHeader('Authorization', 'Token ' + this.token);
  function onLoad (e) {
      if (xhr.readyState === 4) {
          if (xhr.status === 200) {
              let data = JSON.parse(this.response);
              console.log(data.count)
              window.result += data.count
          } 
          else {
            console.log('Please log in to TRIMS.')
            chrome.browserAction.setBadgeText({text: '404'});
            chrome.browserAction.setBadgeBackgroundColor({color: 'red'});
          }
      }
  }
  xhr.onload = onLoad;
  xhr.onerror = function (e) {
      console.error(xhr.statusText);
  };
  xhr.send(null);
}
function trimsRequestCallUser(e) {
  e.preventDefault;
  
  chrome.storage.sync.get([userName], function(result) {
    console.log("The username is " + result[userName])
    window.usrName = result[userName];
  });
  chrome.storage.sync.get([trimsToken], function(result) {
    console.log("The token is " + result[trimsToken])
    window.token = result[trimsToken];
  });
  console.log(window.usrName, window.token)
  if (!window.usrName || !window.token) {
    console.log('Please log in to TRIMS.')
    chrome.browserAction.setBadgeText({text: '404'});
    chrome.browserAction.setBadgeBackgroundColor({color: 'red'});
  }
  else {
    var query = [queryVuln, queryScrp]
    for (q in query) {
      trimsRequest(window.token, query[q] + window.usrName);
    }
    //chrome.browserAction.setBadgeText({text: String(0)});    //this is for notification on the icon 
    chrome.browserAction.setBadgeText({text: String(window.result)});    //this is for notification on the icon   
    chrome.browserAction.setBadgeBackgroundColor({color: 'green'}); 
    window.result = 0
  }
}

//chrome.runtime.onInstalled.addListener(trimsRequestCallUser);
chrome.browserAction.onClicked.addListener(trimsRequestCallUser);

//---------------------- FETCH -------------------------------
/*
function status(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
}

function json(response) {
  return response.json()
}

var apiVulnUrl = 'https://bdsr-uat-trims-web01.dc1.lan/api/vulns/?&status=DRAFT&returned=TRUE&owned_by=odemirok'
function callMe() {
  fetch(apiVulnUrl)
  .then(status)
  .then(json)
  .then(function(data) {
    //console.log('Request succeeded with JSON response', data.count);
    chrome.browserAction.setBadgeText({text: String(data.count)});
  }).catch(function(error) {
    console.log('Request failed', error);
  });
}
chrome.browserAction.onClicked.addListener(callMe);
*/
