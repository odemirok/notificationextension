# notificationExtension

This software is a Google Chrome browser extension to show notifications from TRIMS.

It is created for just exploring whether or not it is practical to use an extension to get notifications from TRIMS.

It doesn't have a pop up page, it only shows the number of DRAFT_RETURNED and SCRAPED_ASSIGNED combined on the icon.

h2. How to install

- Clone the repo.
- Open chrome://extensions/ in the Chrome browser.
- Developer mode: ON
- Load unpacked
- Select the folder consisting of the cloned repo.
- Visit the TRIMS page or just refresh it (It has to be done only at installation).  
- Click on the newly added extension icon (3 times at first installation).

h2. How to use

- Click on the icon.

h2. Troubleshoot

- 404: Visit TRIMS web page and refresh it.
- Shows weird numbers: Wait 1 second in beetween updates.
